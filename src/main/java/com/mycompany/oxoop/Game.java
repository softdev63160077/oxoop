/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.oxoop;

import java.util.Scanner;

/**
 *
 * @author NonWises
 */
public class Game {
    private Player o;
    private Player x;
    private int row;
    private int col;
    private Board board;
    Scanner kb = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }
    
    public void newBoard(){
        this.board = new Board(o, x);
    }
    
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    public void showTurn(){
        Player player = board.getCurrentPlayer();
        System.out.println("Turn"+ player.getSymbol());
    }
    
    public void showTable() {
        char [][] table = board.getTable();
        for ( row = 0; row < table.length; row++) {
            for ( col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }
    
    public void inputRowCol(){
        while (true) {
            System.out.println("Please input row, col:");
            row = kb.nextInt();
            col = kb.nextInt();
            
//            if (checkOutOfRange(row, col)) continue;
//            if (checkDuplicate(row, col)) continue;
            if(board.setRowCol(row, col)){
                return;
            }
//            break;

        }

    }
    public boolean isFinish(){
        if (board.isDraw()||board.isWin()) {
            return true;
        }
        return false;
    }
    
    public void showStat(){
        System.out.println(o);
        System.out.println(x);
    }
    
    public void showResult(){
        if (board.isDraw()) {
            System.out.println("Draw!!!");
        } else if(board.isWin() ){
            System.out.println(board.getCurrentPlayer().getSymbol()+" Win");
        }
    }
    
    public boolean checkContinue(){
        while (true) {            
            System.out.println("Do you want to continue? [y, n]: ");
            char con = kb.next().charAt(0);
            if (con == 'y') {
                return true;
            } else if(con == 'n') {
                return false;
            } 
            
        }
        
    }
    
    
}
