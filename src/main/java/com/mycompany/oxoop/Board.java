/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.oxoop;

/**
 *
 * @author NonWises
 */
public class Board {
    private char [][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    private int row;
    private int col;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    

    public Player getCurrentPlayer() {
        return currentPlayer;
    }


    public Player getO() {
        return o;
    }


    public Player getX() {
        return x;
    }



    public int getCount() {
        return count;
    }
    
    private void switchPlayer(){
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public boolean isWin() {
        if (checkVertical(col)) {
            return true;
        } else if (checkHorizontal(row)) {
            return true;
        } else if (checkDiagonal(currentPlayer.getSymbol())) {
            return true;
        }

        return false;
    }
    public boolean checkVertical(int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public  boolean checkHorizontal(int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public  boolean checkDiagonal(char currentPlayer) {
        if (checkLeftDiagonal(currentPlayer)) {
            return true;
        } else if (checkRightDiagonal(currentPlayer)) {
            return true;
        }
        return false;
    }

    public boolean checkLeftDiagonal(char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public boolean checkRightDiagonal(char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return false;
    }

    public boolean isDraw() {
        if(count == 9) {
            return true;
        }
        return false;
        
    }
    
    
    public boolean setRowCol(int row, int col){
        this.row = row;
        this.col = col;
        if (isWin()||isDraw()) {
            return false;
        }
        if (((row <= 0) || (row > 3)) || ((col <= 0) || (col > 3))) {
            System.out.println("Index out of range, please input in [1,2,3]");
            return false;
        }
        if (checkDuplicate()) return false;
        if(table[row-1][col-1]!='-') return false;
        
        table[row-1][col-1] = currentPlayer.getSymbol();
        if (checkWin()) {
            updateStat();
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            o.Draw();
            x.Draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }
    
    public boolean checkDuplicate() {
        if ((getIndex(row - 1, col - 1) == 'O') || (getIndex(row - 1, col - 1) == 'X')) {
            System.out.println("This index is already taken, please try again.");
            return true;
        }
        return false;
    }
     public char getIndex(int a, int b) {
        return table[a][b];
    }

    public void updateStat() {
        if (this.currentPlayer == o) {
            o.Win();
            x.Loss();
        } else {
            x.Loss();
            o.Win();
        }
    }
    
     public boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }

        return false;
    }

    public boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal() {
        if (checkLeftDiagonal()) {
            return true;
        } else if (checkRightDiagonal()) {
            return true;
        }
        return false;
    }

    public boolean checkLeftDiagonal() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkRightDiagonal() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return false;
    }

    public boolean checkDraw() {
        if(count == 9) {
            return true;
        }
        return false;
    }
}
